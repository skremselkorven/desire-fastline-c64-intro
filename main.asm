// FASTLINE BBS INTRO BY DESIRE
// CODE BY GOLARA IN 2019
// https://gitlab.com/pseregiet/desire-fastline-c64-intro

.var muzak = LoadSid("Fastline_Hiphop.sid")
.var koa = LoadBinary("gfx/intro_bitmap.kla", BF_KOALA)
.var border_color = $01
.var bg_color = $00
.const part1_sprite_pointers = $63f8

:BasicUpstart2(entry)
            .import source "code/entry.asm"
* = $1000 "SID"
            .fill muzak.size, muzak.getData(i)
* = $2000 "CODE"
            .import source "code/intro_irq.asm"
            .import source "code/part1.asm"
            .import source "code/part2.asm"
* = $4000 "VIC: BITMAP"
koa_bitmap:
            .fill koa.getBitmapSize(), koa.getBitmap(i)
* = $6000 "VIC: SCREEN"
koa_screen:
            .fill koa.getScreenRamSize(), koa.getScreenRam(i)
            .align $40
sprites:
            .var desire = LoadPicture("gfx/desire.bmp")
            .for (var s = 0; s < 8; s++)
            {
                .for (var y = 0; y < 21; y++)
                {
                    .for (var x = 0; x < 3; x++)
                    {
                        .byte desire.getSinglecolorByte(s*3+x, y) ^ $ff
                    }
                }
                .byte $00
            }

            .var fastline = LoadPicture("gfx/fastline.bmp")
            .for (var s = 0; s < 8; s++)
            {
                .for (var y = 0; y < 21; y++)
                {
                    .for (var x = 0; x < 3; x++)
                    {
                        .byte fastline.getSinglecolorByte(s*3+x, y) ^ $ff
                    }
                }
                .byte $00
            }

sprite_1:
            .byte $ff,$ff,$ff,$ff,$ff,$ff,$ff,$ff
            .byte $ff,$ff,$ff,$ff,$ff,$ff,$ff,$ff
            .byte $ff,$ff,$ff,$ff,$ff,$ff,$ff,$ff
            .byte $00,$00,$00,$00,$00,$00,$00,$00
            .byte $00,$00,$00,$00,$00,$00,$00,$00
            .byte $00,$00,$00,$00,$00,$00,$00,$00
            .byte $00,$00,$00,$00,$00,$00,$00,$00
            .byte $00,$00,$00,$00,$00,$00,$00,$00


* = $8000 "VIC part2 TEXT CHARSET"
            .var charimg = LoadPicture("gfx/charsscroll.png")
            .for (var x = 0; x < 64; x++)
            {
                .for (var y = 0; y < 8; y++)
                {
                    .byte charimg.getSinglecolorByte(x, y)
                }
            }

            .for (var x = 0; x < 64; x++)
            {
                .for (var y = 8; y < 16; y++)
                {
                    .byte charimg.getSinglecolorByte(x, y)
                }
            }
* = $8400 "VIC part2 SCREEN"
            .import binary "gfx/part2logo_screenram.bin",2
* = $8800 "VIC part2 LOGO CHARSET"
            .import binary "gfx/part2logo_charset.bin",2
            .align $40
sprite_B:
            .var bimg = LoadPicture("gfx/b.png", List().add($b56148, $c161c9, $f7ff6c))
            .for (var y = 0; y < 21; y++)
            {
                .for (var x = 0; x < 3; x++)
                {
                    .byte bimg.getMulticolorByte(x,y)
                }
            }
            .byte $00
sprite_S:
            .var simg = LoadPicture("gfx/s.png", List().add($b56148, $c161c9, $f7ff6c))
            .for (var y = 0; y < 21; y++)
            {
                .for (var x = 0; x < 3; x++)
                {
                    .byte simg.getMulticolorByte(x,y)
                }
            }
            .byte $00

* = * "VIC part2 SKATER"
            .var skimg = LoadPicture("gfx/skater.png", List().add($000000, $ffffff, $362976, $b7c576))
skater_sprites1:
            .for (var i = 0; i < 4; i++)
            {
                .for (var y = 0; y < 21; y++)
                {
                    .byte skimg.getMulticolorByte(i*6 + 0, y)
                    .byte skimg.getMulticolorByte(i*6 + 1, y)
                    .byte skimg.getMulticolorByte(i*6 + 2, y)
                }
                .byte $00
            }
skater_sprites2:
            .for (var i = 0; i < 4; i++)
            {
                .for (var y = 0; y < 21; y++)
                {
                    .byte skimg.getMulticolorByte(i*6 + 3, y)
                    .byte skimg.getMulticolorByte(i*6 + 4, y)
                    .byte skimg.getMulticolorByte(i*6 + 5, y)
                }
                .byte $00
            }
skater_sprites3:
            .for (var i = 0; i < 4; i++)
            {
                .for (var y = 0; y < 21; y++)
                {
                    .byte skimg.getMulticolorByte(i*6 + 0, y+21)
                    .byte skimg.getMulticolorByte(i*6 + 1, y+21)
                    .byte skimg.getMulticolorByte(i*6 + 2, y+21)
                }
                .byte $00
            }
skater_sprites4:
            .for (var i = 0; i < 4; i++)
            {
                .for (var y = 0; y < 21; y++)
                {
                    .byte skimg.getMulticolorByte(i*6 + 3, y+21)
                    .byte skimg.getMulticolorByte(i*6 + 4, y+21)
                    .byte skimg.getMulticolorByte(i*6 + 5, y+21)
                }
                .byte $00
            }

* = * "tables"
.align $100
bbsx:
.byte 175,170,165,161,156,152,147,143,139,135,131,128,125,121,118,116
.byte 113,111,109,107,106,105,104,103,103,103,103,104,105,106,107,109
.byte 111,113,115,118,120,123,126,130,133,137,140,144,148,151,155,159
.byte 163,167,171,175,178,182,185,189,192,195,198,201,204,206,208,211
.byte 212,214,216,217,218,219,219,220,220,220,220,220,219,218,217,216
.byte 215,214,212,210,209,207,205,203,201,199,197,195,193,191,189,187
.byte 185,183,181,179,178,176,175,173,172,171,170,169,168,167,167,167
.byte 166,166,166,166,166,167,167,168,168,169,170,171,171,172,173,174
.byte 175,176,177,178,178,179,180,181,181,182,182,183,183,183,183,183
.byte 182,182,182,181,180,179,178,177,176,175,173,171,170,168,166,164
.byte 162,160,158,156,154,152,150,148,146,144,142,140,139,137,135,134
.byte 133,132,131,130,129,129,129,129,129,130,130,131,132,133,135,137
.byte 138,141,143,145,148,151,154,157,160,164,167,171,174,178,182,186
.byte 190,194,198,201,205,209,212,216,219,223,226,229,231,234,236,238
.byte 240,242,243,244,245,246,246,246,246,245,244,243,242,240,238,236
.byte 233,231,228,224,221,218,214,210,206,202,197,193,188,184,179,175


bbsy:
.byte 62,61,61,60,60,59,58,58,57,57,56,56,55,55,54,54
.byte 53,53,52,52,52,51,51,51,50,50,50,50,50,50,50,50
.byte 50,50,50,50,50,50,50,50,50,51,51,51,52,52,52,53
.byte 53,54,54,55,55,56,56,57,57,58,59,59,60,60,61,62
.byte 62,63,63,64,65,65,66,66,67,68,68,69,69,70,70,71
.byte 71,71,72,72,73,73,73,73,74,74,74,74,74,74,74,74
.byte 74,74,74,74,74,74,74,74,73,73,73,73,72,72,71,71
.byte 71,70,70,69,69,68,68,67,67,66,65,65,64,64,63,62
.byte 62,61,60,60,59,59,58,57,57,56,56,55,55,54,54,53
.byte 53,53,52,52,51,51,51,51,50,50,50,50,50,50,50,50
.byte 50,50,50,50,50,50,50,50,51,51,51,51,52,52,53,53
.byte 53,54,54,55,55,56,56,57,58,58,59,59,60,61,61,62
.byte 62,63,64,64,65,65,66,67,67,68,68,69,69,70,70,71
.byte 71,72,72,72,73,73,73,74,74,74,74,74,74,74,74,74
.byte 74,74,74,74,74,74,74,74,73,73,73,72,72,72,71,71
.byte 70,70,69,69,68,68,67,67,66,66,65,64,64,63,63,62

skater_sinx:
.var tabel = List().add(
  0,0,0,0,0,0,0,0,0,1,1,1,1,2,2,2
, 3,3,4,4,5,5,6,6,7,8,8,9,10,10,11,12
, 13,14,14,15,16,17,18,19,20,21,22,23,24,25,26,28
, 29,30,31,32,34,35,36,37,39,40,42,43,44,46,47,49
, 50,52,53,55,56,58,60,61,63,64,66,68,70,71,73,75
, 76,78,80,82,84,85,87,89,91,93,95,97,99,101,103,104
, 106,108,110,112,114,116,118,120,122,124,126,129,131,133,135,137
, 139,141,143,145,147,149,151,154,156,158,160,162,164,166,168,170
, 173,175,177,179,181,183,185,187,189,192,194,196,198,200,202,204
, 206,208,210,212,214,217,219,221,223,225,227,229,231,233,235,237
, 239,240,242,244,246,248,250,252,254,256,257,259,261,263,265,267
, 268,270,272,273,275,277,279,280,282,283,285,287,288,290,291,293
, 294,296,297,299,300,301,303,304,306,307,308,309,311,312,313,314
, 315,317,318,319,320,321,322,323,324,325,326,327,328,329,329,330
, 331,332,333,333,334,335,335,336,337,337,338,338,339,339,340,340
, 341,341,341,342,342,342,342,343,343,343,343,343,343,343,343,344
)
.fill tabel.size(), tabel.get(i)
skater_sind010:
.for (var i = 0; i < $100; i++)
{
    .if (tabel.get(i) >= 256)
    {
        .byte $f0
    }
    else .if (tabel.get(i)+24 >= 256)
    {
        .byte $a0
    }
    else
    {
        .byte $00
    }
}

colorfade_03:
.byte $0,$0,$9,$2,$8,$a,$f,$7
.byte $7,$f,$a,$8,$2,$9,$0,$0
colorfade_0e:
.byte $0,$0,$0,$0,$9,$2,$4,$a
.byte $a,$4,$2,$9,$0,$0,$0,$0
colorfade_06:
.byte $0,$0,$0,$0,$0,$b,$9,$2
.byte $2,$9,$b,$0,$0,$0,$0,$0
colorfade_05:
.byte $0,$0,$9,$2,$8,$c,$f,$d
.byte $d,$f,$c,$8,$2,$9,$0,$0
colorfade_07:
.byte $0,$0,$0,$b,$c,$8,$f,$7
.byte $7,$f,$8,$c,$b,$0,$0,$0
colorfade_04:
.byte $0,$0,$0,$0,$0,$6,$b,$4
.byte $4,$b,$6,$0,$0,$0,$0,$0

cables_colors:
//.for (var i = 0; i < 5; i++)
{
    .byte $0 + 8
    .byte $0 + 8
    .byte $0 + 8
    .byte $6 + 8
    .byte $3 + 8
    .byte $1 + 8
    .byte $1 + 8
    .byte $3 + 8
}
.for (var i = 0; i <120; i++) { .byte $0+8 }

// some code from part2 that didn't fit before graphics and had to be put here
part2_init:
            ldx #$00
            stx p2c0idx
            stx p2c1idx
            stx p2c2idx
            stx p2c3idx
            stx change_textpage
            stx textpage_replaced
            stx pages_shown
            
            ldx #<colorfade_03
            stx pal1
            ldx #<colorfade_0e
            stx pal2
            ldx #<colorfade_06
            stx pal3
            ldx #<colorfade_04
            stx pal4
            ldx #<colorfade_07
            stx pal5
            ldx #<colorfade_05
            stx pal6

            ldx #127
            stx fld_idx
            ldx #$b0
            stx text_timer
            rts

setup_skater:
            lda #$00
            sta $d016
            lda #$06
            sta $d027 + 4
            sta $d027 + 5
            sta $d027 + 6
            sta $d027 + 7
            lda #$01
            sta $d025
            lda #$07
            sta $d026

            lda text_timer
/*
            cmp #5
            bcs !+
            lda # (skater_sprites1 / $40) + 0
            sta $87f8 + 4
            lda # (skater_sprites2 / $40) + 0
            sta $87f8 + 5
            lda # (skater_sprites3 / $40) + 0
            sta $87f8 + 6
            lda # (skater_sprites4 / $40) + 0
            sta $87f8 + 7
            jmp afteranim
!:
*/
            cmp #100
            bcs !+
            lda anim1:# (skater_sprites1 / $40) + 1 - 1
            sta $87f8 + 4
            lda anim2:# (skater_sprites2 / $40) + 1 - 1
            sta $87f8 + 5
            lda anim3:# (skater_sprites3 / $40) + 1 - 1
            sta $87f8 + 6
            lda anim4:# (skater_sprites4 / $40) + 1 - 1
            sta $87f8 + 7
            jmp afteranim
!:
            cmp #109
            bcs !+
            lda # (skater_sprites1 / $40) + 2
            sta $87f8 + 4
            lda # (skater_sprites2 / $40) + 2
            sta $87f8 + 5
            lda # (skater_sprites3 / $40) + 2
            sta $87f8 + 6
            lda # (skater_sprites4 / $40) + 2
            sta $87f8 + 7
            jmp afteranim
!:
            cmp #118
            bcs !+
            lda # (skater_sprites1 / $40) + 3
            sta $87f8 + 4
            lda # (skater_sprites2 / $40) + 3
            sta $87f8 + 5
            lda # (skater_sprites3 / $40) + 3
            sta $87f8 + 6
            lda # (skater_sprites4 / $40) + 3
            sta $87f8 + 7
            jmp afteranim
!:
            cmp #127
            bcs !+
            lda # (skater_sprites1 / $40) + 2
            sta $87f8 + 4
            lda # (skater_sprites2 / $40) + 2
            sta $87f8 + 5
            lda # (skater_sprites3 / $40) + 2
            sta $87f8 + 6
            lda # (skater_sprites4 / $40) + 2
            sta $87f8 + 7
            jmp afteranim
!:
            cmp #136
            bcs !+
            lda # (skater_sprites1 / $40) + 1
            sta $87f8 + 4
            lda # (skater_sprites2 / $40) + 1
            sta $87f8 + 5
            lda # (skater_sprites3 / $40) + 1
            sta $87f8 + 6
            lda # (skater_sprites4 / $40) + 1
            sta $87f8 + 7
            jmp afteranim
!:
//            cmp #78
//            bne !+
            lda # (skater_sprites1 / $40) + 0
            sta $87f8 + 4
            lda # (skater_sprites2 / $40) + 0
            sta $87f8 + 5
            lda # (skater_sprites3 / $40) + 0
            sta $87f8 + 6
            lda # (skater_sprites4 / $40) + 0
            sta $87f8 + 7
            jmp afteranim
!:

            lda # (skater_sprites1 / $40)
            sta $87f8 + 4
            lda # (skater_sprites2 / $40)
            sta $87f8 + 5
            lda # (skater_sprites3 / $40)
            sta $87f8 + 6
            lda # (skater_sprites4 / $40)
            sta $87f8 + 7
afteranim:
            lda #203
            sta $d001 + (4*2)
            sta $d001 + (5*2)
            lda #203+21
            sta $d001 + (6*2)
            sta $d001 + (7*2)
            lda skaterx1:#00
            sta $d000 + (4*2)
            sta $d000 + (6*2)
            lda skaterx2:#24
            sta $d000 + (5*2)
            sta $d000 + (7*2)
            lda #$f0
            sta $d015
            sta $d01c
            lda dd1010:#00
            sta $d010

            ldx text_timer//skater_gox:#00
            lda skater_sind010, x
            sta dd1010
            lda skater_sinx, x
            sta skaterx1
            clc
            adc #24
            sta skaterx2
            

            rts