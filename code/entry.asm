// FASTLINE BBS INTRO BY DESIRE
// CODE BY GOLARA IN 2019
// https://gitlab.com/pseregiet/desire-fastline-c64-intro

entry:
            sei
            lda #$35
            sta $01

            lda #$7f
            sta $dc0d
            sta $dd0d
            lda $dc0d
            lda $dd0d

            ldx #$ff
            txs

            lda #$00
            jsr muzak.init

            jsr copy_part1bitmap_backup

            lda #<irq_intro_forever
            sta $fffe
            lda #>irq_intro_forever
            sta $ffff
            lda #1
            sta $d012
            lda #$1b
            sta $d011
            
            jsr setup_introbar_sprites

            lda #sprite_F/$40
            sta $7f8 + 0
            lda #sprite_O/$40
            sta $7f8 + 1
            lda #sprite_R/$40
            sta $7f8 + 2
            lda #sprite_E/$40
            sta $7f8 + 3
            lda #sprite_V/$40
            sta $7f8 + 4
            lda #sprite_E/$40
            sta $7f8 + 5
            lda #sprite_R/$40
            sta $7f8 + 6

            lda #$00
            ldx #63
!:          sta $200,x
            dex
            bpl !-

            lda #$ff
            ldx #3*8 - 1
!:
            sta $200,x
            dex 
            bpl !-

            lda #1
            sta $d019
            sta $d01a
            cli
end:
mainloop:   jmp end
//########################################################################
waster3:
            jsr waster_rts
waster2:
            jsr waster_rts
waster1:
            jsr waster_rts
waster_rts: rts
//########################################################################
copy_part1bitmap_backup:
            ldx #$00
!:            
            lda s:$4000, x
            sta d:$a000, x
            inx
            bne !-
            inc s+1
            inc d+1
            lda s+1
            cmp #>$6100
            bne !-
            lda #>$4000
            sta s+1
            lda #>$a000
            sta d+1
            rts
//########################################################################

* = $900 "Color Ram"
koa_d800:
.fill koa.getColorRamSize(), koa.getColorRam(i)
.align $100

bouncesin:
.byte 15,15,15,15,15,15,15,16,16,16,17,17,17,18,19,19
.byte 20,20,21,22,23,24,24,25,26,27,28,29,30,31,33,34
.byte 35,36,38,39,40,42,43,45,46,48,49,51,53,54,56,58
.byte 59,61,63,65,67,68,70,72,74,76,78,80,82,84,86,88
.byte 90,93,95,97,99,101,103,106,108,110,112,115,117,119,121,124
.byte 126,128,131,133,135,138,140,142,145,147,149,152,154,156,159,161
.byte 163,165,168,170,172,175,177,179,181,184,186,188,190,193,195,197
.byte 199,201,203,205,207,209,211,213,215,217,219,221,223,225,227,229

.byte 230,228,227,225,223,221,220,218,216,214,213,211,209,208,206,204
.byte 202,201,199,198,196,194,193,191,190,188,187,186,184,183,182,180
.byte 179,178,177,176,175,174,173,172,171,170,169,169,168,167,167,166
.byte 166,166,165,165,165,165,165,165,165,165,165,165,165,165,166,166
.byte 167,167,168,168,169,170,171,171,172,173,174,175,176,177,179,180
.byte 181,182,184,185,186,188,189,191,192,194,195,197,199,200,202,204
.byte 205,207,209,210,212,214,216,217,219,221,222,224,226,228,229,231
.byte 233,234,236,238,239,241,242,244,246,247,248,250,251,253,254,255

sprite_F:
.var sf = LoadPicture("gfx/F.png")
.for (var y = 0; y < 21; y++)
{
    .byte sf.getSinglecolorByte(0, y)^$ff
    .byte sf.getSinglecolorByte(1, y)^$ff
    .byte sf.getSinglecolorByte(2, y)^$ff
}
.byte $00

sprite_O:
.var so = LoadPicture("gfx/O.png")
.for (var y = 0; y < 21; y++)
{
    .byte so.getSinglecolorByte(0, y)^$ff
    .byte so.getSinglecolorByte(1, y)^$ff
    .byte so.getSinglecolorByte(2, y)^$ff
}
.byte $00

sprite_R:
.var sr = LoadPicture("gfx/R.png")
.for (var y = 0; y < 21; y++)
{
    .byte sr.getSinglecolorByte(0, y)^$ff
    .byte sr.getSinglecolorByte(1, y)^$ff
    .byte sr.getSinglecolorByte(2, y)^$ff
}
.byte $00

sprite_E:
.var se = LoadPicture("gfx/E.png")
.for (var y = 0; y < 21; y++)
{
    .byte se.getSinglecolorByte(0, y)^$ff
    .byte se.getSinglecolorByte(1, y)^$ff
    .byte se.getSinglecolorByte(2, y)^$ff
}
.byte $00

sprite_V:
.var sv = LoadPicture("gfx/V.png")
.for (var y = 0; y < 21; y++)
{
    .byte sv.getSinglecolorByte(0, y)^$ff
    .byte sv.getSinglecolorByte(1, y)^$ff
    .byte sv.getSinglecolorByte(2, y)^$ff
}
.byte $00
