// FASTLINE BBS INTRO BY DESIRE
// CODE BY GOLARA IN 2019
// https://gitlab.com/pseregiet/desire-fastline-c64-intro

irq_intro:
            sta rega1
            stx regx1
            sty regy1

            ldx delayer:#70
            dex
            beq !+
            stx delayer
            jsr muzak.play
            jmp endirqintro
!:
            //triggers at the top of line 1
            //set the bitmap and white border
            lda #$ff
            sta $d015

            lda #bg_color
            sta $d021
            lda #border_color
            sta $d020
            lda #$3b
            sta $d011
            lda #$2
            sta $dd00
            lda #$80
            sta $d018
            lda #$18
            sta $d016

            ldx bar1_:#08
            .for (var i = 0; i < 8; i++)
            {
                stx $d001 + i*2
            }
            jsr border_rasters_intro
            ldx bar1:#08
            dex
!:          cpx $d012
            bne !-
            ldx #25
!:          dex
            bne !-
            lda #$01
            sta $d020

            lda #$1b
            sta $d011
            lda #$3
            sta $dd00
            lda #$14
            sta $d018
            lda basic3:#$08
            sta $d016

            lda bar2:#16
!:          cmp $d012
            bne !-

            lda basic1:#$0e
            sta $d020
            lda basic2:#$06
            sta $d021

            lda bar1
            sec
            sbc #$30
            bcc skip_color_copy
            tax
            lda color_ptr_src_lo, x
            sta $02
            lda color_ptr_src_hi, x
            sta $03
            lda color_ptr_dst_lo, x
            sta $04
            lda color_ptr_dst_hi, x
            sta $05

            ldy #39
!:
            lda ($02), y
            sta ($04), y
            dey
            bpl !-
skip_color_copy:

            lax bar1
            axs #-4
            stx bar1
            stx bar1_
            beq next_step_intro

            lax bar2
            axs #-4
            stx bar2

            lda #00
            sta $d015
            jsr muzak.play
endirqintro:
            inc $d019
            lda rega1:#00
            ldx regx1:#00
            ldy regy1:#00

            rti


next_step_intro:
            lda #<irq_intro2
            sta $fffe
            lda #>irq_intro2
            sta $ffff
            lda #$00
            sta $d015
            lda #bg_color
            sta $d021
            lda #$3b
            sta $d011
            lda #$2
            sta $dd00
            lda #$80
            sta $d018
            lda #$18
            sta $d016
            jmp endirqintro
            
irq_intro2:
            sta rega10
            stx regx10
            sty regy10
            lda #$01
            sta $d020
            jsr muzak.play
            ldx #$ff
            jsr border_rasters_intro

            lda bar3:#00
!:          cmp $d012
            bne !-
            lda #$0e
            sta $d020

            lax bar3
            axs #-4
            stx bar3
            cpx #48
            beq end_of_intro
!:
            lda rega10:#00
            ldx regx10:#00
            ldy regy10:#00
            inc $d019
            rti
end_of_intro:
            lda #<writer_irq
            sta $fffe
            lda #>writer_irq
            sta $ffff
            lda #50
            sta $d012
            jsr setup_after_intro
            jmp !-

