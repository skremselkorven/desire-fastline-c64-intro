// FASTLINE BBS INTRO BY DESIRE
// CODE BY GOLARA IN 2019
// https://gitlab.com/pseregiet/desire-fastline-c64-intro

setup_after_intro:
            lda #$2
            sta $dd00
            lda #$80
            sta $d018
            lda #$18
            sta $d016

            lda #bg_color
            sta $d021
            lda #border_color
            sta $d020

            lda #$08
            .for (var i = 0; i < 8; i++)
            {
                sta $d027 + i
            }
            lda #$0b
            sta $d026
            lda #$0c
            sta $d025

            lda #$00
            sta $7fff
            sta $bfff

            lda #$00
            sta $02
            sta $03
            lda #$3b
            sta $d011
            rts

// MAIN SCREEN IRQ
irq:
            sta rega2
            stx regx2
            sty regy2
        
            lda #$37
            sta $d011

            lda ($02),y
            
            lda #border_color
            sta $d021

            ldx colorfade_index:#90
            dex
            cpx #40
            bne dont_swap_logos
            lda top_ptr
            ldy bottom_ptr
            sta bottom_ptr
            sty top_ptr
dont_swap_logos:
            stx colorfade_index
            .for (var i = 0; i < 8; i++)
            {
                lda colorfade, x                
                sta $d027 + i
                inx
            }

            lda #$ff
            sta $d015
            sta $d01b
            lda #$00
            sta $d010
            sta $d01d
            
            ldx top_ptr:#(sprites / $40)
            ldy #255
            .for (var i = 0; i < 8; i++)
            {
                stx part1_sprite_pointers + i
                .if (i != 7) { inx }
                lda #24 + (i*24)
                sta $d000 + (i*2)
                sty $d001 + (i*2)
            }
            ldy bottom_logo_index:#00
            jsr swing_sprite_logo
            inc bottom_logo_index

            jsr muzak.play
            lda #$3b
            sta $d011

            lda #30
!:          cmp $d012
            bne !-
            
            ldy top_logo_index:#$40
            jsr swing_sprite_logo
            inc top_logo_index

            ldx bottom_ptr:#(sprites / $40) + 8
            ldy #23
            .for (var i = 0; i < 8; i++)
            {
                stx part1_sprite_pointers + i
                .if (i != 7) { inx }
                sty $d001 + (i*2)
            }


            lda #<writer_irq
            sta $fffe
            lda #>writer_irq
            sta $ffff
            lda #50
            sta $d012


            inc $d019

            lda rega2:#00
            ldx regx2:#00
            ldy regy2:#00
            rti

swing_sprite_logo:
            //Y index to sine table preset
            lda sinx, y
            clc
            ldy #$00
            .for (var i = 0; i < 8; i++)
            {
                sta $d000 + (i*2)
                .if (i != 7) { adc #24 }
                bcc !+
                ldy #$ff << i+1
                clc
!:
            }
            sty $d010
            rts

.macro change_border_color(line, color)
{
            lda #line
!:          cmp $d012
            bne !-
            jsr waster3
            nop
            nop
            nop
c:          lda #color
            sta $d020
}

.macro wait_for_a_line(l)
{
            lda #l
!:          cmp $d012
            bne !-
}

.var vsy = List().add($4e,$66,$7e,$96,$ae,$c6)
.var vsw = List().add($62,$7a,$92,$aa,$c2,$da)
.var brl = List().add($4a,$62,$66,$6a,$72,$76,$92,$9a,$9e,$b2,$ca,$ce,$d2,$da,$de)
.var brc = List().add($00,$0e,$01,$00,$0e,$01,$00,$0e,$01,$00,$0e,$01,$00,$0e,$01)
border_rasters_intro:
            .for (var i = 0; i < brl.size(); i++)
            {
                cpx #brl.get(i)+3
                bcc !+
                change_border_color(brl.get(i), brc.get(i))
!:
            }
            rts


bitmap_fadeout:
            jsr clear_wall
bc:
            .for (var i = 0; i < brl.size(); i++)
            {
                change_border_color(brl.get(i), brc.get(i))
            }
            jsr clear_wall
            jsr clear_wall
            jsr clear_wall
            inc bcis
            lda bcis:#00
            and #1
            bne fadeoutendirq
            ldx bci:#00
            lda colorfade00, x
            sta bc + 14 + (18*00)
            sta bc + 14 + (18*03)
            sta bc + 14 + (18*06)
            sta bc + 14 + (18*09)
            sta bc + 14 + (18*12)
            lda colorfade0e, x
            sta bc + 14 + (18*01)
            sta bc + 14 + (18*04)
            sta bc + 14 + (18*07)
            sta bc + 14 + (18*10)
            sta bc + 14 + (18*13)
            lda colorfade01, x
            sta bc + 14 + (18*02)
            sta bc + 14 + (18*05)
            sta bc + 14 + (18*08)
            sta bc + 14 + (18*11)
            sta bc + 14 + (18*14)
            inx
            cpx #32
            beq fadetout
            stx bci
fadeoutendirq:
            jsr muzak.play
            inc $d019
            rti
fadetout:
            jsr part2_init
            lda #<part2_irq
            sta $fffe
            lda #>part2_irq
            sta $ffff
            lda #246
            sta $d012
            lda #$00
            sta $d011
            lda #$88
            sta $d018
            jsr muzak.play

            lda #<part2_outofirqloop
            sta mainloop+1
            lda #>part2_outofirqloop
            sta mainloop+2

            lda #$00
            ldx #$00
            stx $d015
            
!:
            //sta $8400,x
            //sta $8500,x
            //sta $8600,x
            //sta $8700,x
            sta $d800,x
            sta $d900,x
            sta $da00,x
            sta $db00,x
            inx
            bne !-
!:
            lda #$08
!:
            sta $d800,x
            inx
            cpx #240
            bne !-
            sta $d800

            lda #<scrolltext
            sta text1
            sta text2
            lda #>scrolltext
            sta text1+1
            sta text2+1
            lda #<($8400 + 280)
            sta $04
            lda #>($8400 + 280)
            sta $05
            lda #<($8400 + 320)
            sta $06
            lda #>($8400 + 320)
            sta $07


            inc $d019
            rti

writer_irq:
            sta rega3
            stx regx3
            sty regy3

            lda #$00
            sta $d010
            sta $d01b

            lda ($02), y
            lda ($02), y
            lda ($02), y
            lda ($02), y

            lda #bg_color
            sta $d021
            
            .for (var i = 0; i < brl.size(); i++)
            {
                change_border_color(brl.get(i), brc.get(i))
            }
            //inc effect1_time
            //lda effect1_time:#00
            ldx colorfade_index
            cpx #90
            bne !+
            inc effect2_time
            lda effect2_time:#00
            cmp #3
            beq gotofadeout
!:
            lda #<irq
            sta $fffe
            lda #>irq
            sta $ffff
            lda #249
            sta $d012

endofwriter:
            inc $d019
            lda rega3:#00
            ldx regx3:#00
            ldy regy3:#00

            rti
gotofadeout:
            lda #<bitmap_fadeout
            sta $fffe
            lda #>bitmap_fadeout
            sta $ffff
            lda #50
            sta $d012
            jmp endofwriter

clear_wall:
            ldy wallidx:#00
            lax wallsine, y

            lda lines_lo, x
            sta $20
            lda lines_hi, x
            sta $21

            ldy #00
            ldx #32
            clc
!:
            lda #$00
            sta ($20), y
            tya
            adc #8
            tay
            dex
            bne !-
            ldx #8
            inc $21
            clc
!:
            lda #$00
            sta ($20), y
            tya
            adc #8
            tay
            dex
            bne !-
!:
            inc wallidx
            rts

irq_intro_forever:
            sta regfa
            stx regfx
            sty regfy
            lda #$ff
            sta $d015
            
ee:         .for (var i = 0; i < 8; i++)
            {
                ldx bounceidx:#5*i
                lda bouncesin,x
                sta $d001 + i*2
                inc bounceidx
            }
            jsr muzak.play

            lda ee+1
            bne !+
            lda #<irq_intro
            sta $fffe
            lda #>irq_intro
            sta $ffff
            lda #$ff
            sta $d01d
            lda #$200/$40
            .for (var i = 0; i < 7; i++)
            {
                sta $7f8 + i
            }
!:

            lda regfa:#00
            ldx regfx:#00
            ldy regfy:#00
            inc $d019
            rti
init_part1:


setup_introbar_sprites:
            lda #$ff
            sta $d015
            .for (var i = 0; i < 8; i++)
            {
                lda #24 + 48*i
                sta $d000 + (i*2)
            }
            lda #%11100000
            sta $d010

            lda #border_color
            .for (var i = 0; i < 8; i++)
            {
                sta $d027 + i
            }
            ldy #sprite_1/$40
            .for (var i = 0; i < 8; i++)
            {
                sty part1_sprite_pointers + i
            }
            rts








colorfade:
.byte $0,$0,$0,$0,$0,$0,$0,$0
.byte $0,$0,$0,$0,$0,$0,$0,$0
.byte $0,$0,$0,$0,$0,$0,$0,$0
.byte $0,$9,$2,$4,$c,$3,$d,$1
.byte $1,$1,$1,$1,$1,$1,$1,$1
.byte $1,$1,$1,$1,$1,$1,$1,$1
.byte $1,$1,$1,$1,$1,$1,$1,$1
.byte $1,$1,$1,$1,$1,$1,$1,$1
.byte $1,$1,$1,$1,$1,$1,$1,$1
.byte $1,$1,$1,$1,$1,$1,$1,$1
.byte $1,$1,$1,$1,$1,$1,$1,$1
.byte $1,$1,$1,$1,$1,$1,$1,$1
.byte $1,$d,$3,$c,$4,$2,$9,$0
.byte $0,$0,$0,$0,$0,$0,$0,$0
.byte $0,$0,$0,$0,$0,$0,$0,$0
.byte $0,$0,$0,$0,$0,$0,$0,$0
.byte $0,$0,$0,$0,$0,$0,$0,$0
.byte $0,$0,$0,$0,$0,$0,$0,$0
.byte $0,$0,$0,$0,$0,$0,$0,$0
.byte $0,$0,$0,$0,$0,$0,$0,$0
.byte $0,$0,$0,$0,$0,$0,$0,$0
.byte $0,$0,$0,$0,$0,$0,$0,$0
.byte $0,$0,$0,$0,$0,$0,$0,$0
.byte $0,$0,$0,$0,$0,$0,$0,$0
.byte $0,$0,$0,$0,$0,$0,$0,$0
.byte $0,$0,$0,$0,$0,$0,$0,$0
.byte $0,$0,$0,$0,$0,$0,$0,$0
.byte $0,$0,$0,$0,$0,$0,$0,$0
.byte $0,$0,$0,$0,$0,$0,$0,$0
.byte $0,$0,$0,$0,$0,$0,$0,$0
.byte $0,$0,$0,$0,$0,$0,$0,$0
.byte $0,$0,$0,$0,$0,$0,$0,$0

//.print "" + toHexString(* - colorfade)

.align $100

color_ptr_src_lo:
.for (var y = 0 ; y < 25; y++)
{
    .for (var i = 0 ; i < 8; i++)
    {
        .byte <(koa_d800 + (y*40))
    }
}
color_ptr_src_hi:
.for (var y = 0 ; y < 25; y++)
{
    .for (var i = 0 ; i < 8; i++)
    {
        .byte >(koa_d800 + (y*40))
    }
}
color_ptr_dst_lo:
.for (var y = 0 ; y < 25; y++)
{
    .for (var i = 0 ; i < 8; i++)
    {
        .byte <($d800 + (y*40))
    }
}
color_ptr_dst_hi:
.for (var y = 0 ; y < 25; y++)
{
    .for (var i = 0 ; i < 8; i++)
    {
        .byte >($d800 + (y*40))
    }
}

lines_lo:
.for (var y = 0; y < 25; y++)
{
    .for (var i = 0; i < 8; i++)
    {
        .byte <($4000 + y*320 + i)
    }
}
.align $100
lines_hi:
.for (var y = 0; y < 25; y++)
{
    .for (var i = 0; i < 8; i++)
    {
        .byte >($4000 + y*320 + i)
    }
}
.align $100
lines_lo_dst:
.for (var y = 0; y < 25; y++)
{
    .for (var i = 0; i < 8; i++)
    {
        .byte <($a000 + y*320 + i)
    }
}
.align $100
lines_hi_dst:
.for (var y = 0; y < 25; y++)
{
    .for (var i = 0; i < 8; i++)
    {
        .byte >($a000 + y*320 + i)
    }
}

.align $100
wallsine:
.for (var i = 0; i < 100; i++)
{
    .byte 100-i
    .byte 100+i
}
.align $100
sinx:
.byte 88,84,81,78,75,72,69,66,63,60,57,54,52,49,47,44
.byte 42,40,38,36,34,32,31,30,28,27,26,25,25,24,24,24
.byte 24,24,24,24,25,26,26,27,29,30,31,33,35,36,38,40
.byte 43,45,47,50,52,55,58,61,64,67,70,73,76,79,82,85
.byte 88,91,95,98,101,104,107,110,113,116,118,121,124,126,129,131
.byte 133,136,138,140,141,143,144,146,147,148,149,150,150,151,151,151
.byte 151,151,151,151,150,149,148,147,146,145,143,142,140,138,136,134
.byte 132,129,127,124,122,119,116,114,111,108,105,102,98,95,92,89
.byte 86,83,80,77,73,70,67,64,61,59,56,53,51,48,46,43
.byte 41,39,37,35,33,32,30,29,28,27,26,25,24,24,24,24
.byte 24,24,24,25,25,26,27,28,29,31,32,34,35,37,39,42
.byte 44,46,49,51,54,57,59,62,65,68,71,74,77,80,84,87
.byte 90,93,96,99,102,105,108,111,114,117,120,123,125,128,130,132
.byte 135,137,139,140,142,144,145,146,148,149,149,150,151,151,151,151
.byte 151,151,151,150,150,149,148,147,145,144,143,141,139,137,135,133
.byte 131,128,126,123,121,118,115,112,109,106,103,100,97,94,91,88
fldtab:
.byte 1,1,1,1,1,1,1,2,2,2,3,3,4,4,5,5
.byte 6,7,7,8,9,10,11,12,13,14,15,16,17,18,19,20
.byte 22,23,24,25,27,28,30,31,33,34,35,37,39,40,42,43
.byte 45,47,48,50,52,53,55,57,58,60,62,64,65,67,69,71
.byte 72,74,76,78,79,81,83,85,86,88,90,92,93,95,97,98
.byte 100,101,103,105,106,108,109,111,112,114,115,116,118,119,120,122
.byte 123,124,125,127,128,129,130,131,132,133,134,135,135,136,137,138
.byte 138,139,140,140,141,141,142,142,142,143,143,143,143,143,143,143
.byte 143,143,143,143,143,143,143,142,142,142,141,141,140,140,139,138
.byte 138,137,136,135,135,134,133,132,131,130,129,128,127,125,124,123
.byte 122,120,119,118,116,115,114,112,111,109,108,106,105,103,101,100
.byte 98,97,95,93,92,90,88,86,85,83,81,79,78,76,74,72
.byte 71,69,67,65,64,62,60,58,57,55,53,52,50,48,47,45
.byte 43,42,40,39,37,35,34,33,31,30,28,27,25,24,23,22
.byte 20,19,18,17,16,15,14,13,12,11,10,9,8,7,7,6
.byte 5,5,4,4,3,3,2,2,2,1,1,1,1,1,1,1

colorfade00:
.byte $0,$6,$b,$4,$4,$4,$4,$4
.byte $4,$2,$9,$0,$0,$0,$0,$0
.byte $0,$0,$0,$0,$0,$0,$0,$0
.byte $0,$0,$0,$0,$0,$0,$0,$0
colorfade01:
.byte $1,$d,$3,$c,$4,$4,$4,$4
.byte $4,$2,$9,$0,$0,$0,$0,$0
.byte $0,$0,$0,$0,$0,$0,$0,$0
.byte $0,$0,$0,$0,$0,$0,$0,$0
colorfade0e:
.byte $e,$4,$4,$4,$4,$4,$4,$4
.byte $4,$2,$9,$0,$0,$0,$0,$0
.byte $0,$0,$0,$0,$0,$0,$0,$0
.byte $0,$0,$0,$0,$0,$0,$0,$0